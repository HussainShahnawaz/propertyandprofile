package com.profile.test;

import com.profile.utils.profileUtility;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import java.util.Properties;

public class profileTest {

    private Properties prop = profileUtility.loadEnvProperties();
    @Test
    public void runprofile(){
        System.out.println(prop.toString());
        WebDriver driver;
        WebDriverManager.chromedriver().setup();
        driver=new ChromeDriver();
        driver.get(prop.getProperty("url"));
        driver.quit();
    }


}
